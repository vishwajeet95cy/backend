const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const exercisesRouter = require('./routes/exercises')
const usersRouter = require('./routes/users')

require('dotenv').config();

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))

mongoose.connect(process.env.DB_HOST, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }, function (err, db) {
  if (err) throw err;
  console.log("Database Connected!");
});

app.use('/exercises', exercisesRouter);
app.use('/users', usersRouter);

app.get('/', (req, res) => {
  res.send('Welcom to the Exercise Backend')
})

app.listen(process.env.PORT, () => {
  console.log(`Server is running on port: http://localhost:${process.env.PORT}`);
});
